﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex6
{
    class Program
    {
        static void Main(string[] args)
        {
            var kms = 0;
            const double miles = 0.621371; 

            Console.WriteLine("Welcome please enter the amount of kms you would like to change to miles");
            kms = int.Parse(Console.ReadLine());

            Console.WriteLine($"Here is the kms converted to miles {kms * miles}");

                
        }
    }
}
