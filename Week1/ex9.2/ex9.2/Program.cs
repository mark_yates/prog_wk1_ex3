﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex9._2
{
    class Program
    {
        static void Main(string[] args)
        {
            var length = 0;
            var breadth = 0;

            Console.WriteLine("please enter rectangle length");
            length = int.Parse(Console.ReadLine());

            Console.WriteLine("Please enter rectangle breadth");
            breadth = int.Parse(Console.ReadLine());

            Console.WriteLine($"Here is your result {(length*2) * (breadth*2)}");
        }
    }
}
