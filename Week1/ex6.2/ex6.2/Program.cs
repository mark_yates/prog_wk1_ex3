﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex6._2
{
    class Program
    {
        static void Main(string[] args)
        {
            var miles = 0;
            const double kms = 1.609344;

            Console.WriteLine("Welcome please enter the amount of miles you would like to change to kms");
            miles = int.Parse(Console.ReadLine());

            Console.WriteLine($"Here is the kms converted to miles {miles * kms}");

        }
    }
}
