﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk2ex3
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("Please enter if you need miles or kms converted");
            var measurement = Console.ReadLine();

            switch (measurement)
            {
                case "miles":
                    Console.WriteLine("Please enter the number of miles wanted converted to kms");
                    var a = int.Parse(Console.ReadLine());
                    var b = 1.609344;
                    Console.WriteLine($"Here is what you are after {a * b}");
                    break;

                case "kms":
                    Console.WriteLine("Please enter the number of kms wanted converted to miles");
                    var c = int.Parse(Console.ReadLine());
                    var d = 0.621371;
                    Console.WriteLine($"Here is what you are after {c * d}");
                    break;

                default:
                    Console.WriteLine("no no you know nothing");
                    break;

            }
        }
    }
}
